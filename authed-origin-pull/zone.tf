###############################################################################
# Optional Variables
###############################################################################

variable "zone_lookup" {
  type        = string
  default     = true
  description = "Lookup the DNS zone ID from its name."
}

###############################################################################
# Data Sources
###############################################################################

data "cloudflare_zone" "object" {
  name    = var.zone_lookup ? var.parent : null
  zone_id = var.zone_lookup ? null : var.parent
}

###############################################################################
# Outputs
###############################################################################

output "zone_lookup" {
  value = var.zone_lookup
}

###############################################################################

output "zone_id" {
  value = data.cloudflare_zone.object.id
}

output "zone_name" {
  value = data.cloudflare_zone.object.name
}

output "zone_plan" {
  value = data.cloudflare_zone.object.plan
}

output "zone_paused" {
  value = data.cloudflare_zone.object.paused
}

output "zone_status" {
  value = data.cloudflare_zone.object.status
}

output "zone_account_id" {
  value = data.cloudflare_zone.object.account_id
}

output "zone_name_servers" {
  value = data.cloudflare_zone.object.name_servers
}

output "zone_vanity_name_servers" {
  value = data.cloudflare_zone.object.vanity_name_servers
}

###############################################################################
