###############################################################################
# Required Variables
###############################################################################

variable "parent" {
  type        = string
  description = "The parent identifier. This must be a zone ID or zone name depending on the value of 'zone_lookup'."
}

###############################################################################
# Optional Variables
###############################################################################

variable "enabled" {
  type        = bool
  default     = true
  description = "Whether or not to enable Authenticated Origin Pulls on the given zone or hostname."
}

variable "hostname" {
  type        = string
  default     = null
  description = "Specify a hostname to enable Per-Hostname Authenticated Origin Pulls on."
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_authenticated_origin_pulls" "object" {
  enabled                                = var.enabled
  zone_id                                = data.cloudflare_zone.object.id
  hostname                               = var.hostname
  authenticated_origin_pulls_certificate = local.certificate_generate ? cloudflare_authenticated_origin_pulls_certificate.object[0].id : null
}

###############################################################################
# Outputs
###############################################################################

output "parent" {
  value = var.parent
}

###############################################################################

output "enabled" {
  value = var.enabled
}

output "hostname" {
  value = var.hostname
}

###############################################################################
