###############################################################################
# Optional Variables
###############################################################################

variable "tls_ca_cert_pem" {
  type        = string
  default     = null
  description = "The CA certificate to use in PEM format. Must be specified if 'tls_ca_cert_generate' is 'false'."
}

variable "tls_ca_cert_generate" {
  type        = bool
  default     = true
  description = "Should the CA certificate be automatically generated? Ignored if 'certificate_generate' is 'false'."
}

###############################################################################

variable "tls_ca_cert_common_name_prefix" {
  type        = string
  default     = "Cloudflare Authenticated Origin Pulls for"
  description = "Prefixed to either the hostname or zone name to form the CA certificate subject common name."
}

###############################################################################

variable "tls_ca_cert_o" {
  type        = string
  default     = null
  description = "Optional organisation for generated CA certificate. Uses 'tls_cert_o' by default."
}

variable "tls_ca_cert_ou" {
  type        = string
  default     = null
  description = "Optional organisational unit for generated CA certificate. Uses 'tls_cert_ou' by default."
}

variable "tls_ca_cert_valid_hours" {
  type        = number
  default     = 87600
  description = "Number of hours, after initial issuing, that the certificate will remain valid for."
}

variable "tls_ca_cert_renewal_hours" {
  type        = number
  default     = 17520
  description = "The resource will consider the certificate to have expired the given number of hours before its actual expiry time. This can be useful to deploy an updated certificate in advance of the expiration of the current certificate."
}

###############################################################################
# Locals
###############################################################################

locals {
  tls_ca_cert_o           = local.tls_ca_cert_generate ? try(coalesce(var.tls_cert_o, var.tls_ca_cert_o), null) : null
  tls_ca_cert_ou          = local.tls_ca_cert_generate ? try(coalesce(var.tls_cert_ou, var.tls_ca_cert_ou), null) : null
  tls_ca_cert_pem         = local.tls_ca_cert_generate ? tls_self_signed_cert.ca[0].cert_pem : var.tls_ca_cert_pem
  tls_ca_cert_generate    = local.certificate_generate && var.tls_ca_cert_generate
  tls_ca_cert_common_name = format("%s %s", var.tls_ca_cert_common_name_prefix, local.certificate_subject)
}

###############################################################################
# Resources
###############################################################################

resource "tls_self_signed_cert" "ca" {
  count                 = local.tls_ca_cert_generate ? 1 : 0
  private_key_pem       = local.tls_ca_key_private_pem
  early_renewal_hours   = var.tls_ca_cert_renewal_hours
  validity_period_hours = var.tls_ca_cert_valid_hours
  is_ca_certificate     = true

  allowed_uses = [
    "cert_signing"
  ]

  subject {
    common_name         = local.tls_ca_cert_common_name
    organization        = local.tls_ca_cert_o
    organizational_unit = local.tls_ca_cert_ou
  }
}

###############################################################################
# Outputs
###############################################################################

output "tls_ca_cert_pem" {
  value = local.tls_ca_cert_pem
}

output "tls_ca_cert_generate" {
  value = local.tls_ca_cert_generate
}

###############################################################################

output "tls_ca_cert_o" {
  value = local.tls_ca_cert_o
}

output "tls_ca_cert_ou" {
  value = local.tls_ca_cert_ou
}

###############################################################################

output "tls_ca_cert_id" {
  value = length(tls_self_signed_cert.ca) == 1 ? tls_self_signed_cert.ca[0].id : null
}

output "tls_ca_cert_renew_due" {
  value = length(tls_self_signed_cert.ca) == 1 ? tls_self_signed_cert.ca[0].ready_for_renewal : null
}

output "tls_ca_cert_valid_from" {
  value = length(tls_self_signed_cert.ca) == 1 ? tls_self_signed_cert.ca[0].validity_start_time : null
}

output "tls_ca_cert_valid_hours" {
  value = length(tls_self_signed_cert.ca) == 1 ? tls_self_signed_cert.ca[0].validity_period_hours : null
}

output "tls_ca_cert_renewal_hours" {
  value = length(tls_self_signed_cert.ca) == 1 ? tls_self_signed_cert.ca[0].early_renewal_hours : null
}

###############################################################################
