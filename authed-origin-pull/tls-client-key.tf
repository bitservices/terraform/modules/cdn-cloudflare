###############################################################################
# Optional Variables
###############################################################################

variable "tls_client_key_rsa_bits" {
  type        = number
  default     = 4096
  description = "The size of the generated RSA key. Ignored if 'tls_client_key_algorithm' is not 'RSA'."
}

variable "tls_client_key_algorithm" {
  type        = string
  default     = null
  description = "Name of the algorithm to use when generating the client certificate private key. Uses 'tls_key_algorithm' by default."
}

variable "tls_client_key_ecdsa_curve" {
  type        = string
  default     = "P384"
  description = "The name of the elliptic curve to use. Ignored if 'tls_client_key_algorithm' is not 'ECDSA'."
}

###############################################################################
# Locals
###############################################################################

locals {
  tls_client_key_rsa_bits    = local.tls_client_key_algorithm == "ECDSA" ? null : var.tls_client_key_rsa_bits
  tls_client_key_algorithm   = coalesce(var.tls_client_key_algorithm, var.tls_key_algorithm)
  tls_client_key_ecdsa_curve = local.tls_client_key_algorithm == "ECDSA" ? var.tls_client_key_ecdsa_curve : null
}

###############################################################################
# Resources
###############################################################################

resource "tls_private_key" "client" {
  count       = local.certificate_generate ? 1 : 0
  rsa_bits    = local.tls_client_key_rsa_bits
  algorithm   = local.tls_client_key_algorithm
  ecdsa_curve = local.tls_client_key_ecdsa_curve
}

###############################################################################
# Outputs
###############################################################################

output "tls_client_key_rsa_bits" {
  value = local.tls_client_key_rsa_bits
}

output "tls_client_key_algorithm" {
  value = local.tls_client_key_algorithm
}

output "tls_client_key_ecdsa_curve" {
  value = local.tls_client_key_ecdsa_curve
}

###############################################################################

output "tls_client_key_id" {
  value = length(tls_private_key.client) == 1 ? tls_private_key.client[0].id : null
}

output "tls_client_key_public_md5" {
  value = length(tls_private_key.client) == 1 ? tls_private_key.client[0].public_key_fingerprint_md5 : null
}

output "tls_client_key_public_pem" {
  value = length(tls_private_key.client) == 1 ? tls_private_key.client[0].public_key_pem : null
}

output "tls_client_key_private_pem" {
  value     = length(tls_private_key.client) == 1 ? tls_private_key.client[0].private_key_pem : null
  sensitive = true
}

output "tls_client_key_public_sha256" {
  value = length(tls_private_key.client) == 1 ? tls_private_key.client[0].public_key_fingerprint_sha256 : null
}

output "tls_client_key_private_pkcs8" {
  value     = length(tls_private_key.client) == 1 ? tls_private_key.client[0].private_key_pem_pkcs8 : null
  sensitive = true
}

output "tls_client_key_public_openssh" {
  value = length(tls_private_key.client) == 1 ? tls_private_key.client[0].public_key_openssh : null
}

output "tls_client_key_private_openssh" {
  value     = length(tls_private_key.client) == 1 ? tls_private_key.client[0].private_key_openssh : null
  sensitive = true
}

###############################################################################
