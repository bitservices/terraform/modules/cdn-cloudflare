###############################################################################
# Optional Variables
###############################################################################

variable "tls_key_algorithm" {
  type        = string
  default     = "ECDSA"
  description = "Name of the default algorithm to use when generating private keys."
}

###############################################################################

variable "tls_cert_o" {
  type        = string
  default     = null
  description = "Default optional organisation for generated certificates."
}

variable "tls_cert_ou" {
  type        = string
  default     = null
  description = "Default optional organisational unit for generated certificates."
}

###############################################################################
