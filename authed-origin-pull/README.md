<!---------------------------------------------------------------------------->

# authed-origin-pull

#### [Cloudflare] settings for [Authenticated Origin Pull]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/cloudflare//authed-origin-pull`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_authed_origin_pull" {
  source = "gitlab.com/bitservices/cdn/cloudflare//authed-origin-pull"
  parent = "bitservices.io"
}
```

<!---------------------------------------------------------------------------->

[Cloudflare]:                https://www.cloudflare.com/
[Authenticated Origin Pull]: https://developers.cloudflare.com/ssl/origin-configuration/authenticated-origin-pull/

<!---------------------------------------------------------------------------->
