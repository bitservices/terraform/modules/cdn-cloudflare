###############################################################################
# Optional Variables
###############################################################################

variable "tls_ca_key_generate" {
  type        = bool
  default     = true
  description = "Should the CA certificate private key be automatically generated? Ignored if either 'certificate_generate' or 'tls_ca_cert_generate' are 'false'."
}

variable "tls_ca_key_private_pem" {
  type        = string
  default     = null
  description = "The CA certificate private key to use in PEM format. Must be specified if 'tls_ca_key_generate' is 'false'."
}

###############################################################################

variable "tls_ca_key_rsa_bits" {
  type        = number
  default     = 4096
  description = "The size of the generated RSA key. Ignored if 'tls_ca_key_algorithm' is not 'RSA'."
}

variable "tls_ca_key_algorithm" {
  type        = string
  default     = null
  description = "Name of the algorithm to use when generating the CA certificate private key. Uses 'tls_key_algorithm' by default."
}

variable "tls_ca_key_ecdsa_curve" {
  type        = string
  default     = "P384"
  description = "The name of the elliptic curve to use. Ignored if 'tls_ca_key_algorithm' is not 'ECDSA'."
}

###############################################################################
# Locals
###############################################################################

locals {
  tls_ca_key_generate    = local.tls_ca_cert_generate && var.tls_ca_key_generate
  tls_ca_key_rsa_bits    = local.tls_ca_key_algorithm == "ECDSA" ? null : var.tls_ca_key_rsa_bits
  tls_ca_key_algorithm   = coalesce(var.tls_ca_key_algorithm, var.tls_key_algorithm)
  tls_ca_key_ecdsa_curve = local.tls_ca_key_algorithm == "ECDSA" ? var.tls_ca_key_ecdsa_curve : null
  tls_ca_key_private_pem = local.tls_ca_key_generate ? tls_private_key.ca[0].private_key_pem : var.tls_ca_key_private_pem
}

###############################################################################
# Resources
###############################################################################

resource "tls_private_key" "ca" {
  count       = local.tls_ca_key_generate ? 1 : 0
  rsa_bits    = local.tls_ca_key_rsa_bits
  algorithm   = local.tls_ca_key_algorithm
  ecdsa_curve = local.tls_ca_key_ecdsa_curve
}

###############################################################################
# Outputs
###############################################################################

output "tls_ca_key_generate" {
  value = local.tls_ca_key_generate
}

output "tls_ca_key_private_pem" {
  value     = local.tls_ca_key_private_pem
  sensitive = true
}

###############################################################################

output "tls_ca_key_id" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].id : null
}

output "tls_ca_key_rsa_bits" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].rsa_bits : null
}

output "tls_ca_key_algorithm" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].algorithm : null
}

output "tls_ca_key_public_md5" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].public_key_fingerprint_md5 : null
}

output "tls_ca_key_public_pem" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].public_key_pem : null
}

output "tls_ca_key_ecdsa_curve" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].ecdsa_curve : null
}

output "tls_ca_key_public_sha256" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].public_key_fingerprint_sha256 : null
}

output "tls_ca_key_private_pkcs8" {
  value     = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].private_key_pem_pkcs8 : null
  sensitive = true
}

output "tls_ca_key_public_openssh" {
  value = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].public_key_openssh : null
}

output "tls_ca_key_private_openssh" {
  value     = length(tls_private_key.ca) == 1 ? tls_private_key.ca[0].private_key_openssh : null
  sensitive = true
}

###############################################################################

