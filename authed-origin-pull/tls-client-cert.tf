###############################################################################
# Optional Variables
###############################################################################

variable "tls_client_cert_o" {
  type        = string
  default     = null
  description = "Optional organisation for generated client certificate. Uses 'tls_cert_o' by default."
}

variable "tls_client_cert_ou" {
  type        = string
  default     = null
  description = "Optional organisational unit for generated client certificate. Uses 'tls_cert_ou' by default."
}

variable "tls_client_cert_valid_hours" {
  type        = number
  default     = 17520
  description = "Number of hours, after initial issuing, that the certificate will remain valid for."
}

variable "tls_client_cert_renewal_hours" {
  type        = number
  default     = 8760
  description = "The resource will consider the certificate to have expired the given number of hours before its actual expiry time. This can be useful to deploy an updated certificate in advance of the expiration of the current certificate."
}

###############################################################################
# Locals
###############################################################################

locals {
  tls_client_cert_o  = try(coalesce(var.tls_cert_o, var.tls_client_cert_o), null)
  tls_client_cert_ou = try(coalesce(var.tls_cert_ou, var.tls_client_cert_ou), null)
}

###############################################################################
# Resources
###############################################################################

resource "tls_cert_request" "client" {
  count           = local.certificate_generate ? 1 : 0
  private_key_pem = tls_private_key.client[0].private_key_pem

  subject {
    common_name         = local.certificate_subject
    organization        = local.tls_client_cert_o
    organizational_unit = local.tls_client_cert_ou
  }
}

###############################################################################

resource "tls_locally_signed_cert" "client" {
  count                 = local.certificate_generate ? 1 : 0
  ca_cert_pem           = local.tls_ca_cert_pem
  cert_request_pem      = tls_cert_request.client[0].cert_request_pem
  ca_private_key_pem    = local.tls_ca_key_private_pem
  early_renewal_hours   = var.tls_client_cert_renewal_hours
  validity_period_hours = var.tls_client_cert_valid_hours

  allowed_uses = [
    "client_auth",
    "key_encipherment",
    "digital_signature"
  ]
}

###############################################################################
# Outputs
###############################################################################

output "tls_client_cert_o" {
  value = local.tls_client_cert_o
}

output "tls_client_cert_ou" {
  value = local.tls_client_cert_ou
}

###############################################################################

output "tls_client_cert_csr_id" {
  value = length(tls_cert_request.client) == 1 ? tls_cert_request.client[0].id : null
}

output "tls_client_cert_csr_pem" {
  value = length(tls_cert_request.client) == 1 ? tls_cert_request.client[0].cert_request_pem : null
}

###############################################################################

output "tls_client_cert_id" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].id : null
}

output "tls_client_cert_pem" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].cert_pem : null
}

output "tls_client_cert_valid_to" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].validity_end_time : null
}

output "tls_client_cert_renew_due" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].ready_for_renewal : null
}

output "tls_client_cert_valid_from" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].validity_start_time : null
}

output "tls_client_cert_valid_hours" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].validity_period_hours : null
}

output "tls_client_cert_renewal_hours" {
  value = length(tls_locally_signed_cert.client) == 1 ? tls_locally_signed_cert.client[0].early_renewal_hours : null
}

###############################################################################
