###############################################################################
# Optional Variables
###############################################################################

variable "certificate_generate" {
  type        = bool
  default     = true
  description = "Should a custom certificate be generated for authenticated origin pulls or the default Cloudflare certificate? Ignored if 'hostname' is set since a custom certificate is then required."
}

###############################################################################
# Locals
###############################################################################

locals {
  certificate_type     = var.hostname == null ? "per-zone" : "per-hostname"
  certificate_subject  = var.hostname == null ? data.cloudflare_zone.object.name : var.hostname
  certificate_generate = var.hostname == null ? var.certificate_generate : true
}

###############################################################################
# Resources
###############################################################################

resource "cloudflare_authenticated_origin_pulls_certificate" "object" {
  count       = local.certificate_generate ? 1 : 0
  type        = local.certificate_type
  zone_id     = data.cloudflare_zone.object.id
  certificate = tls_locally_signed_cert.client[0].cert_pem
  private_key = tls_private_key.client[0].private_key_pem
}

###############################################################################
# Outputs
###############################################################################

output "certificate_type" {
  value = local.certificate_type
}

output "certificate_subject" {
  value = local.certificate_subject
}

output "certificate_generate" {
  value = local.certificate_generate
}

###############################################################################
