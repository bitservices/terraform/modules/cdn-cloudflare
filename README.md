<!---------------------------------------------------------------------------->

# cdn (cloudflare)

<!---------------------------------------------------------------------------->

## Description

Manage [Cloudflare] Content Delivery Network (CDN) components.

<!---------------------------------------------------------------------------->

## Modules

 * [authed-origin-pull](authed-origin-pull/README.md) - [Cloudflare] settings for [Authenticated Origin Pull].

<!---------------------------------------------------------------------------->

[Cloudflare]:                https://www.cloudflare.com/
[Authenticated Origin Pull]: https://developers.cloudflare.com/ssl/origin-configuration/authenticated-origin-pull/

<!---------------------------------------------------------------------------->
